const express = require("express");
const { authentication } = require("../middlewares/Auth/authentication");

const {
  createUser,
  getAllUser,
  getDetailUser,
  updateUser,
  deleteUser,
  login,
} = require("../controllers/user");

const router = express.Router();

router.post("/signup", createUser);
router.get("/", authentication, getAllUser);
router.post("/login", login);
router.put("/:id", authentication, updateUser);
router.get("/:id", authentication, getDetailUser);
router.delete("/:id", authentication, deleteUser);

module.exports = router;

// ""
