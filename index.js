const express = require("express");
const app = express();
const { titik, sequelize } = require("./models");
const cors = require("cors");
const routes = require("./routes/index");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(routes);
app.get("/", (req, res) => {
  res.send("Hello, Welcome!");
});
const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`server running on port ${port}`));
