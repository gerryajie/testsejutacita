const { user } = require("../models");

const { createToken, encodePin, compare } = require("../utils/index");
const sequelize = require("sequelize");

class User {
  async getAllUser(req, res, next) {
    try {
      const userId = req.userData.id;
      const role = req.userData.role;

      const data = await user.findAll({
        attributes: {
          exclude: ["password", "createdAt", "updatedAt", "deletedAt"],
        },
      });
      if (role === "user") {
        return res.status(401).json({ errors: ["You are not Admin"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async getDetailUser(req, res, next) {
    try {
      const userId = req.userData.id;
      const role = req.userData.role;

      const data = await user.findOne({
        where: { id: req.params.id },
        attributes: {
          exclude: ["password", "createdAt", "updatedAt", "deletedAt"],
        },
      });
      if (role === "user") {
        return res.status(401).json({ errors: ["You are not Admin"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async createUser(req, res, next) {
    try {
      const { username, password } = req.body;
      const hashPassword = encodePin(password);

      await user.create({
        username,

        password: hashPassword,
      });

      const data = await user.findOne({
        where: {
          username: username,
        },
        attributes: {
          exclude: ["password", "createdAt", "updatedAt", "deletedAt"],
        },
      });

      // const payload = {
      //   id: data.dataValues.id,
      //   username: data.dataValues.username,
      //   role: data.dataValues.role,
      // };
      // // const token = createToken(payload);
      // console.log("disini", data);

      /* Function to send welcome email to new user */

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      console.log("ini error bosku", error);
      res
        .status(500)
        .json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async updateUser(req, res, next) {
    try {
      const userId = req.userData.id;
      const role = req.userData.role;
      console.log("ini role", role);

      if (role === "user") {
        return res.status(401).json({ errors: ["You are not Admin"] });
      }
      await user.update(req.body, {
        where: { id: req.params.id },
      });

      const data = await user.findOne({
        where: {
          id: +userId,
        },
      });

      // if (!data) {
      //   return res.status(404).json({ errors: ["account not found"] });
      // }

      res.status(201).json({ success: true, message: ["Success Update Data"] });
    } catch (error) {
      res
        .status(500)
        .json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async deleteUser(req, res, next) {
    try {
      const userId = req.userData.id;
      const role = req.userData.role;
      const deletedData = await user.destroy({
        where: {
          id: req.params.id,
        },
        force: true,
      });
      if (role === "user") {
        return res.status(401).json({ errors: ["You are not Admin"] });
      }

      if (deletedData.id != +userId) {
        return res
          .status(404)
          .json({ success: false, message: ["User has been deleted"] });
      }

      res
        .status(200)
        .json({ success: true, message: ["Success deleting data"] });
    } catch (error) {
      res
        .status(500)
        .json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async login(req, res, next) {
    try {
      const username = req.body.username;
      const password = req.body.password;

      const loginUser = await user.findOne({
        where: {
          username: username,
        },
      });
      const data = await user.findOne({
        where: {
          username: username,
        },
        attributes: {
          exclude: ["password", "createdAt", "updatedAt", "deletedAt"],
        },
      });

      if (loginUser == null) {
        return res.status(401).json({
          success: false,
          errors: ["Please Input the correct username and password"],
        });
      }

      const hashPass = loginUser.dataValues.password;
      const compareResult = compare(password, hashPass);

      if (!compareResult) {
        return res.status(401).json({
          success: false,
          errors: ["Please input the correct username and password"],
        });
      }

      const payload = {
        id: loginUser.dataValues.id,
        username: loginUser.dataValues.username,
        role: loginUser.dataValues.role,
      };
      const token = createToken(payload);
      res.status(200).json({
        success: true,
        token: token,
        data: data,
      });
    } catch (error) {
      res
        .status(500)
        .json({ success: false, errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new User();
